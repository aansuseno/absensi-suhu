<?php

namespace App\Controllers;
use App\Models\AdminModel;

class Home extends BaseController
{
	public function __construct() {
		$this->moo = new AdminModel();
	}

	public function index()
	{
		if(count($this->moo->ambil()) == 0){
			return redirect()->to('/pemasangan');
		}
		return redirect()->to('/login');
	}

	public function register()
	{
		return view('daftar');
	}

	public function registerProses()
	{
		if (count($this->moo->ambilKondisi(['username' => $_POST['username']])) != 0) {
			session()->setFlashdata('pesan', 'Username sudah digunakan, coba yang lain.');
			session()->setFlashdata('lama', $_POST);
			return redirect()->to('daftar');
		}
		$dataInput = [
			'nama' => $_POST['nama'],
			'username' => $_POST['username'],
			'no_wa' => $_POST['no_wa'],
			'role' => $_POST['role'],
			'password' => md5($_POST['password']),
		];
		$this->moo->isi($dataInput);

		session()->set(['username7' => $_POST['username'], 'daftar' => true]);

		return redirect()->to('berhasil-daftar');
	}

	public function registerBerhasil()
	{
		if (!session()->get('daftar') || session()->get('daftar') == null) {
			session()->setFlashdata('pesan', 'Halaman belum bisa diakses.');
			return redirect()->to('daftar');
		}
		$data['user'] = $this->moo->ambil(['username' => session()->get('username7')]);
		$data['root'] = $this->moo->ambil(['role' => 'root']);
		return view('berhasilDaftar', $data);
	}

	public function registerEdit()
	{
		if (!session()->get('daftar') || session()->get('daftar') == null) {
			session()->setFlashdata('pesan', 'Halaman tidak bisa diakses.');
			return redirect()->to('daftar');
		}
		$data['user'] = $this->moo->ambil(['username' => session()->get('username7')]);
		return view('editDaftar', $data);
	}

	public function registerEditProses()
	{
		if (!session()->get('daftar') || session()->get('daftar') == null) {
			session()->setFlashdata('pesan', 'Halaman tidak bisa diakses.');
			return redirect()->to('daftar');
		}

		$user = $this->moo->ambil(['id' => $_POST['id']]);

		// validasi
		if (md5($_POST['password']) != $user['password']) {
			session()->setFlashdata('pesan', 'Password salah. Silakan coba lagi.');
			session()->setFlashdata('lama', $_POST);
			return redirect()->to('edit-daftar');
		}
		if ($_POST['username'] != $user['username']) {
			if (count($this->moo->ambilKondisi(['username' => $_POST['username']])) != 0) {
				session()->setFlashdata('pesan', 'Username sudah digunakan, coba yang lain.');
				session()->setFlashdata('lama', $_POST);
				return redirect()->to('edit-daftar');
			}
		}
		
		$dataInput = [
			'nama' => $_POST['nama'],
			'username' => $_POST['username'],
			'no_wa' => $_POST['no_wa'],
			'role' => $_POST['role'],
		];
		$this->moo->ubah($dataInput, ['id' => $_POST['id']]);
		session()->set(['username7' => $_POST['username'], 'daftar' => true]);
		session()->setFlashdata('pesan', 'Data berhasil diedit, silakan tunggu konfirmasi admin.');
		return redirect()->to('edit-daftar');
	}
}
