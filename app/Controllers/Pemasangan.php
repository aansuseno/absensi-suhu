<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\PengaturanModel;

class Pemasangan extends BaseController
{
	public function __construct() {
		$this->madmin = new AdminModel();
		$this->mpengaturan = new PengaturanModel();
	}

	public function index()
	{
		return view('pemasangan/install');
	}

	public function proses()
	{
		$dataAdmin = [
			'nama' => $_POST['nama'],
			'username' => $_POST['username'],
			'no_wa' => $_POST['no_wa'],
			'password' => md5($_POST['password']),
			'role' => 'root',
			'aktif' => 'y'
		];
		$this->madmin->isi($dataAdmin);
		$this->mpengaturan->isi(['nama_sekolah' => $_POST['nama_sekolah']]);
		echo "oke";
	}
}
