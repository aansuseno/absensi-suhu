<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AdminModel;

class Login extends BaseController
{
	public function __construct() {
		$this->madmin = new AdminModel();
	}

	public function index()
	{
		return view('login', ['wa_admin' => $this->madmin->ambil(['role' => 'root'])['no_wa']]);
	}

	public function cek()
	{
		$dataMasuk = [
			'username' => $_POST['username'],
			'password' => md5($_POST['password'])
		];
		$user = $this->madmin->ambil($dataMasuk);
		if($this->madmin->ambil($dataMasuk) == null) {
			session()->setFlashdata('pesan', 'Login gagal. Sandi atau username salah. Jika Anda merasa sudah benar artinya akun Anda sedang diberhentikan.');
			return redirect()->to('/login');
		}
		if($user['aktif'] == 'y' && $user['arsip'] == 'n') {
			session()->set($user);
			session()->set(['login' => true]);
			if ($user['role'] == 'root') {
				return redirect()->to('/root');
			} else if ($user['role'] == 'admin') {
				return redirect()->to('/admin');
			} else if ($user['role'] == 'petugas') {
				return redirect()->to('/petugas');
			}
		} else {
			session()->setFlashdata('pesan', 'Login gagal. Akun tidak aktif.');
			return redirect()->to('/login');
		}
	}
}
