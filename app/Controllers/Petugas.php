<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\SiswaModel;
use App\Models\RecordModel;
use App\Models\AdminModel;

class Petugas extends BaseController
{
	public function __construct() {
		$this->msiswa = new SiswaModel();
		$this->madmin = new AdminModel();
		$this->mrecord = new RecordModel();
		$this->db = db_connect();
	}

    public function index()
    {
        $data = [
			'halaman' => 'Pindai Suhu'
		];
		return view('petugas/scan', $data);
    }

	public function scanManual()
	{
		$data = [
			'halaman' => 'Pindai Suhu Manual'
		];
		return view('petugas/scanManual', $data);
	}

	public function scanManualProses()
	{
		$siswa = $this->msiswa->ambil(['nis' => $_POST['nis'], 'arsip' => 0]);
		if ($siswa == null) {
			session()->setFlashdata('pesan', 'Siswa dengan NIS '.$_POST['nis'].' tidak ditemukan.');
			session()->setFlashdata('lama', $_POST);
			return redirect()->to('/petugas/scanManual');
		}
		$dataInput = [
			'admin_id' => session()->id,
			'siswa_id' => $siswa['id'],
			'suhu' => $_POST['suhu']
		];
		$this->mrecord->isi($dataInput);
		session()->setFlashdata('pesan', 'Berhasil.');
		return redirect()->to('/petugas/scanManual');
	}

	public function scanHariIni()
	{
		$query = "SELECT 
		s.nama as nama,
		s.nis as nis,
		r.suhu as suhu,
		r.created_at as waktu,
		r.id as id
		FROM record AS r
		LEFT JOIN siswa AS s ON s.id = r.siswa_id
		WHERE r.created_at > '".date('Y-m-d 00:00:00')."' 
		AND r.admin_id = ".session()->id." 
		ORDER BY r.id DESC";
		$record = $this->db->query($query)->getResult('array');
		$data = [
			'halaman' => 'Scan Hari Ini',
			'record' => $record
		];
		return view('petugas/hariIni', $data);
	}

	public function scanHariIniEdit()
	{
		$dataEdit = [
			'suhu' => $_POST['suhu'],
		];
		$this->mrecord->ubah($dataEdit,['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Suhu berhasil diedit.');
		return redirect()->to('/petugas/hari-ini');
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to('/login');
	}

	public function scanProses()
	{
		$siswa = $this->msiswa->ambil(['token' => $_GET['token']]);
		if ($siswa != null) {
			return view('petugas/hasilScan', $siswa);
		}
		return 'Siswa tidak ditemukan. <br>
		<button class="btn btn-secondary mt-5 col-12" onclick="$(\'#input-suhu-block\').animate({\'left\': \'-100vw\'}, 500);$(\'#input-suhu-block\').html(\'proses...\');">TUTUP</button>';
	}

	public function scanKirimProses()
	{
		$dataInput = [
			'admin_id' => session()->id,
			'siswa_id' => $_GET['id'],
			'suhu' => $_GET['suhu']
		];
		$this->mrecord->isi($dataInput);
	}

	public function editProfil()
	{
		$data = [
			'halaman' => 'Edit Profil',
			'dataAdmin' => $this->madmin->ambil(['id' => session()->id]),
		];
		return view('petugas/editProfil', $data);
	}

	public function editProfilProses()
	{
		$admin = $this->madmin->ambil(['id' => session()->get('id')]);
		if ($_POST['username'] != $admin['username']) {
			if(count($this->madmin->ambilKondisi(['username' => $_POST['username']])) > 0) {
				session()->setFlashdata('pesan', 'Username sudah digunakan. Silakan coba yang lain.');
				session()->setFlashdata('lama', $_POST);
				return redirect()->to('/petugas/profil');
			}
		}
		$dataEdit = [
			'username' => $_POST['username'],
			'nama' => $_POST['nama'],
			'no_wa' => $_POST['no_wa']
		];
		$this->madmin->ubah($dataEdit, ['id' => $admin['id']]);

		session()->setFlashdata('pesan', 'Profil berhasil diperbarui.');
		return redirect()->to('/petugas/profil');
	}
}
