<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\SiswaModel;
use App\Models\RecordModel;
use PHPExcel;
use PHPExcel_IOFactory;

class Admin extends BaseController
{

	public function __construct() {
		$this->madmin = new AdminModel();
		$this->msiswa = new SiswaModel();
		$this->mrecord = new RecordModel();
	}

	public function index()
	{
		$data = [
			'halaman' => 'Beranda',
		];
		return view('admin/home', $data);
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to('/login');
	}

	// petugas
	public function petugasPradaftar()
	{
		$data = [
			'halaman' => 'Petugas Pradaftar',
			'petugasPradaftar' => $this->madmin->ambilKondisi(['aktif' => 'n', 'role' => 'petugas']),
		];
		return view('admin/petugasPradaftar', $data);
	}

	public function petugasHapus()
	{
		$this->madmin->hapus(['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Data berhasil dihapus.');
		return redirect()->to('/admin/petugas-pradaftar');
	}
	
	public function petugasAktifkan()
	{
		$this->madmin->ubah(['aktif' => 'y'],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Petugas berhasil diaktifkan. Cek di halaman <a href="'.base_url().'/admin/petugas-aktif">petugas aktif</a>. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi bahwa akun sudah aktif.');
		return redirect()->to('/admin/petugas-pradaftar');
	}

	public function petugasAktif()
	{
		$data = [
			'halaman' => 'Petugas Aktif',
			'petugasAktif' => $this->madmin->ambilKondisi(['aktif' => 'y', 'arsip' => 'n', 'role' => 'petugas']),
		];
		return view('admin/petugasAktif', $data);
	}

	public function petugasArsipkan()
	{
		$this->madmin->ubah(['arsip' => 'y'],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Petugas berhasil diarsipkan. Cek di halaman <a href="'.base_url().'/admin/petugas-arsip">petugas arsip</a>. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi bahwa akun sudah diarsipkan.');
		return redirect()->to('/admin/petugas-aktif');
	}

	public function petugasArsip()
	{
		$data = [
			'halaman' => 'Petugas Arsip',
			'petugasArsip' => $this->madmin->ambilKondisi(['aktif' => 'y', 'arsip' => 'y', 'role' => 'petugas']),
		];
		return view('admin/petugasArsip', $data);
	}

	public function petugasBukaArsip()
	{
		$this->madmin->ubah(['arsip' => 'n'],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Petugas berhasil dibuka arsipkan. Cek di halaman <a href="'.base_url().'/admin/petugas-aktif">petugas aktif</a>. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi bahwa akun sudah aktif kembali.');
		return redirect()->to('/admin/petugas-arsip');
	}
	// end petugas

	public function editProfil()
	{
		$data = [
			'halaman' => 'Edit Profil',
			'dataAdmin' => $this->madmin->ambil(['id' => session()->get('id')]),
		];
		return view('admin/editProfil', $data);
	}

	public function editProfilProses()
	{
		$admin = $this->madmin->ambil(['id' => session()->get('id')]);
		if ($_POST['username'] != $admin['username']) {
			if(count($this->madmin->ambilKondisi(['username' => $_POST['username']])) > 0) {
				session()->setFlashdata('pesan', 'Username sudah digunakan. Silakan coba yang lain.');
				session()->setFlashdata('lama', $_POST);
				return redirect()->to('/admin/edit-profil');
			}
		}
		$dataEdit = [
			'username' => $_POST['username'],
			'nama' => $_POST['nama'],
			'no_wa' => $_POST['no_wa']
		];
		$this->madmin->ubah($dataEdit, ['id' => $admin['id']]);

		session()->setFlashdata('pesan', 'Profil berhasil diperbarui.');
		return redirect()->to('/admin/edit-profil');
	}

	public function editPassword()
	{
		$data = [
			'halaman' => 'Edit Password',
		];
		return view('admin/editPassword', $data);
	}

	public function editPasswordProses()
	{
		$admin = $this->madmin->ambil(['id' => session()->get('id')]);
		$dataEdit = [
			'password' => md5($_POST['password']),
		];
		$this->madmin->ubah($dataEdit, ['id' => $admin['id']]);

		session()->setFlashdata('pesan', 'Password berhasil diperbarui.');
		return redirect()->to('/admin/edit-password');
	}

	// kelola siswa
	public function siswaImport()
	{
		$data = [
			'halaman' => 'Import Siswa'
		];
		return view('admin/siswaImport', $data);
	}

	public function siswaImportProses()
	{
		$file = $this->request->getFile('data_excel');
		$ext = $file->getClientExtension();
		if ($ext == 'xls') {
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		} else {
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		$spreadsheet = $render->load($file);
		$hasilArray = $spreadsheet->getActiveSheet()->toArray();

		$gagal = [];
		foreach ($hasilArray as $key => $kolom) {
			if (
				count($this->msiswa->ambilKondisi(['nis' => $kolom[0]])) ||
				$kolom[0] == '' ||
				$kolom[0] == null ||
				$kolom[1] == '' ||
				$kolom[1] == null ||
				$kolom[2] == '' ||
				$kolom[2] == null ||
				!is_numeric($kolom[0])
			) {
				array_push($gagal, [$kolom[0], $key+1]);
				continue;
			}
			
			$dataInput = [
				'nis' => $kolom[0],
				'token' => 'rahasia_'.$kolom[0].md5($kolom[0]),
				'nama' => $kolom[1],
				'kelas' => strtoupper($kolom[2]),
				'admin_id' => session()->get('id'),
			];
			$this->msiswa->isi($dataInput);
		}
		$pesan = 'Data berhasil ditambah.';
		if (count($gagal) > 0) {
			$pesan .= ' <br><br>Siswa dengan NIS:<br>';
			foreach ($gagal as $g) {
				$pesan .= $g[0].', data baris ke-'.$g[1].'<br>';
			}
			$pesan .= 'gagal diupload dikarenakan oleh beberapa hal berikut: NIS sudah tersedia, nama atau kelas tidak terisi, atau NIS bukan angka.';
		}
		session()->setFlashdata('pesan', $pesan);
		return redirect()->to('/admin/siswa/import');
	}

	public function siswa()
	{
		$data = [
			'halaman' => 'Tampil Kelas',
			'kelas' => $this->msiswa->ambilGrup('kelas', ['arsip' => 0]),
		];
		return view('admin/siswa', $data);
	}

	public function siswaPerkelas($kelas = '')
	{
		$siswa = $this->msiswa->ambilKondisi(['arsip' => 0, 'kelas' => $kelas]);
		if (count($siswa) == 0) {
			session()->setFlashdata('pesan', 'Kelas yang anda inputkan tidak ditemukan.');
			return redirect()->to('/admin/siswa');
		}
		$data = [
			'halaman' => 'Kelas',
			'kelas' => $kelas,
			'siswa' => $siswa,
		];
		return view('admin/siswaPerkelas', $data);
	}

	public function siswaArsipkan()
	{
		$this->msiswa->ubah(['arsip' => 1],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Siswa berhasil diarsipkan.');
		return redirect()->to('/admin/kelas/'.$_POST['kelas']);
	}

	public function siswaHapus()
	{
		$this->msiswa->hapus(['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Siswa berhasil dihapus.');
		return redirect()->to('/admin/kelas/'.$_POST['kelas']);
	}

	public function siswaEdit($nis)
	{
		$data = [
			'halaman' => 'Edit Siswa',
			'siswa' => $this->msiswa->ambil(['nis' => $nis]),
		];
		if ($data['siswa'] == null) {
			session()->setFlashdata('pesan', 'Siswa tidak ditemukan.');
			return redirect()->to('/admin/siswa');
		}
		return view('admin/siswaEdit', $data);
	}

	public function siswaEditProses()
	{
		$siswaLama = $this->msiswa->ambil(['id' => $_POST['id']]);
		if ($_POST['nis'] != $siswaLama['nis']) {
			if ($this->msiswa->ambilKondisi(['nis' => $_POST['nis']]) != null) {
				session()->setFlashdata('pesan', 'NIS sudah digunakan.');
				session()->setFlashdata('lama', $_POST);
				return redirect()->to('/admin/siswa/edit/'.$siswaLama['nis']);
			}
		}
		$dataEdit = [
			'nama' => $_POST['nama'],
			'kelas' => strtoupper($_POST['kelas']),
			'nis' => $_POST['nis'],
			'token' => 'rahasia_'.$_POST['nis'].md5($_POST['nis']),
		];
		$this->msiswa->ubah($dataEdit,['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Data berhasil diedit.');
		return redirect()->to('/admin/siswa/edit/'.$_POST['nis']);
	}

	public function siswaEditKelas()
	{
		$dataEdit = [
			'kelas' => strtoupper($_POST['kelas_baru']),
		];
		$this->msiswa->ubah($dataEdit,['kelas' => $_POST['kelas_lama'], 'arsip' => 0]);
		session()->setFlashdata('pesan', 'Kelas berhasil diedit.');
		return redirect()->to('/admin/kelas/'.strtoupper($_POST['kelas_baru']));
	}

	public function siswaTambah()
	{
		$data = [
			'halaman' => 'Tambah Siswa',
		];
		return view('admin/siswaTambah', $data);
	}

	public function siswaTambahProses()
	{
		if ($this->msiswa->ambil(['nis' => $_POST['nis']]) != null) {
			session()->setFlashdata('pesan', 'NIS sudah digunakan.');
			session()->setFlashdata('lama', $_POST);
			return redirect()->to('/admin/siswa/tambah');
		}
		$dataInput = [
			'nis' => $_POST['nis'],
			'token' => 'rahasia_'.$_POST['nis'].md5($_POST['nis']),
			'nama' => $_POST['nama'],
			'kelas' => strtoupper($_POST['kelas']),
			'admin_id' => session()->get('id'),
		];
		$this->msiswa->isi($dataInput);
		session()->setFlashdata('pesan', 'Data berhasil ditambahkan. Bisa dicek di kelas <a href="/admin/kelas/'.strtoupper($_POST['kelas']).'">'.strtoupper($_POST['kelas']).'</a>.');
		return redirect()->to('/admin/siswa/tambah');
	}

	public function siswaCari()
	{
		$siswa = $this->msiswa->ambilKondisi(['arsip' => 0, 'nama' => 'xxxxxxxxx']);
		if (isset($_POST['cari'])) {
			$db = db_connect();
			$query = "SELECT * FROM siswa WHERE (nama LIKE '%{$_POST['cari']}%' OR nis = '{$_POST['cari']}') AND arsip = 0";
			$siswa = $db->query($query)->getResult('array');
		}
		$data = [
			'halaman' => 'Cari Siswa',
			'siswa' => $siswa,
		];
		return view('admin/siswaCari', $data);
	}

	public function siswaSuhu($nis = 0)
	{
		$siswa = $this->msiswa->ambil(['nis' => $nis]);
		$data = [
			'halaman' => 'Suhu Siswa',
			'record' => $this->mrecord->ambilKondisi(['siswa_id' => $siswa['id']]),
			'siswa' => $siswa,
		];
		if ($siswa == null) {
			session()->setFlashdata('pesan', 'Siswa tidak ditemukan.');
			return redirect()->to('/admin/siswa');
		}
		return view('admin/siswaSuhu', $data);
	}

	public function siswaHapusRecord()
	{
		$this->mrecord->hapus(['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Record berhasil dihapus.');
		return redirect()->to('/admin/siswa/suhu/'.$_POST['nis']);
	}

	public function siswaPrintRecord()
	{
		$tgl_mulai = strtotime($_POST['tgl_dari']);
		$tgl_sampai = strtotime($_POST['tgl_sampai']);
		$selisih_hari = round(($tgl_sampai - $tgl_mulai) / (60*60*24));
		$tgl_mulai = date('Y-m-d',$tgl_mulai);
		$tabel = "<table border='1'>
					<tr>
						<th>No.</th>
						<th>NIS</th>
						<th>Nama</th>";
		for ($i=0; $i <= $selisih_hari ; $i++) {
			$tgl =  date('d M', strtotime("{$tgl_mulai} + {$i} days"));
			$tabel.= "<th>{$tgl}</th>";
		}
		$tabel.= "</tr>";

		$db = db_connect();
		$siswa = $this->msiswa->ambilKondisi(['kelas' => $_POST['kelas'], 'arsip' => 0]);
		foreach ($siswa as $key => $s) {
			$nomor = $key+1;
			$tabel .= "<tr>
			<td>{$nomor}</td>
			<td>{$s['nis']}</td>
			<td>{$s['nama']}</td>";
			for ($i=0; $i <= $selisih_hari ; $i++) {
				$tgl =  date('Y-m-d', strtotime("{$tgl_mulai} + {$i} days"));
				$harian = $db->query("SELECT suhu as suhu FROM record where siswa_id = {$s['id']}  and cast(created_at as date) = '{$tgl}' group by cast(created_at as date)")->getResult('array');
				if (count($harian) != 0) {
					$tabel .= "<td>{$harian[0]['suhu']}</td>";
				} else {
					$tabel .= "<td></td>";
				}
			}
			$tabel .= "</tr>";
		}
		$tabel.="</table>";

		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
		$spreadsheet = $reader->loadFromString($tabel);

		try {
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
			$writer->save($_POST['kelas'].'.xlsx');
			$content = file_get_contents($_POST['kelas'].'.xlsx');
		} catch(Exception $e) {
			exit($e->getMessage());
		}
		
		header("Content-Disposition: attachment; filename=".$_POST['kelas'].'.xlsx');
		
		unlink($_POST['kelas'].'.xlsx');
		exit($content);
	}
	// end kelola siswa
}
