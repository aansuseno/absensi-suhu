<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AdminModel;

class Root extends BaseController
{
	public function __construct() {
		$this->madmin = new AdminModel();
	}

	public function index()
	{
		$data = [
			'halaman' => 'Admin Aktif',
			'userAktif' => $this->madmin->ambilKondisi([
				'aktif' => 'y',
				'arsip' => 'n',
				'role !=' => 'root'
			]),
		];
		return view('root/adminAktif', $data);
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to('/login');
	}

	public function editProfil()
	{
		$data = [
			'halaman' => 'Profil',
			'root' => $this->madmin->ambil(['id' => session()->id]),
		];
		return view('root/editProfil', $data);
	}

	public function editDataProses()
	{
		// if$_POST
		if ($_POST['username'] != $_POST['username_lama']) {
			if(count($this->madmin->ambilKondisi(['username' => $_POST['username']])) > 0) {
				session()->setFlashdata('pesan', 'Username sudah digunakan. Silakan coba yang lain.');
				return redirect()->to('/root/profil');
			}
		}
		$dataEdit = [
			'username' => $_POST['username'],
			'nama' => $_POST['nama'],
			'no_wa' => $_POST['no_wa']
		];
		$this->madmin->ubah($dataEdit, ['id' => $_POST['id']]);

		session()->setFlashdata('pesan', 'Data berhasil diubah.');
		return redirect()->to('/root/profil');
	}
	
	public function editPwProses()
	{
		$dataEdit = [
			'password' => md5($_POST['password']),
		];
		$this->madmin->ubah($dataEdit, ['id' => $_POST['id']]);

		$pw = substr($_POST['password'], 0, 3);
		$jml = strlen($_POST['password']) - 3;
		$sisaPw = "*";
		for ($i=1; $i < $jml; $i++) { 
			$sisaPw .= "*";
		}
		session()->setFlashdata('pesan', 'Password anda adalah '.$pw.$sisaPw);
		return redirect()->to('/root/profil');
	}

	public function petugasPradaftar()
	{
		$data = [
			'halaman' => 'User Pradaftar',
			'belumAktif' => $this->madmin->ambilKondisi(['aktif' => 'n']),
		];
		return view('root/petugasPradaftar', $data);
	}

	public function petugasHapus()
	{
		$this->madmin->hapus(['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Data berhasil dihapus.');
		return redirect()->to('/root/user-pradaftar');
	}

	public function petugasAktifkan()
	{
		$this->madmin->ubah(['aktif' => 'y'],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'User berhasil diaktifkan. Cek di halaman <a href="'.base_url().'/root">admin aktif</a>. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi akun sudah aktif.');
		return redirect()->to('/root/user-pradaftar');
	}

	public function petugasArsipkan()
	{
		$this->madmin->ubah(['arsip' => 'y'],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'User berhasil diarsipkan. Cek di halaman <a href="'.base_url().'/root/user-arsip">admin arsip</a>. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi akun sudah diarsipkan.');
		return redirect()->to('/root');
	}

	public function petugasReset()
	{
		$this->madmin->ubah(['password' => md5('admin123')],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Password berhasil di-reset. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi password sudah di-reset menjadi <b>admin123</b>.');
		return redirect()->to('/root');
	}

	public function petugasArsip()
	{
		$data = [
			'halaman' => 'Arsip',
			'userArsip' => $this->madmin->ambilKondisi([
				'arsip' => 'y',
				'role !=' => 'root'
			]),
		];
		return view('root/adminArsip', $data);
	}

	public function petugasBukaArsip()
	{
		$this->madmin->ubah(['arsip' => 'n'],['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'User berhasil dibuka arsip. Cek di halaman <a href="'.base_url().'">admin aktif</a>. Silakan hubungi <a href="https://wa.me/'.$this->madmin->ambil(['id' => $_POST['id']])['no_wa'].'" target="_blank">'.$this->madmin->ambil(['id' => $_POST['id']])['nama'].'</a> untuk konfirmasi akun sudah diaktifkan kembali.');
		return redirect()->to('/root/user-arsip');
	}

	public function panduan()
	{
		$data = [
			'halaman' => 'Panduan'
		];
		return view('root/panduan', $data);
	}
}
