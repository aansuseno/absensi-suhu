<?php

namespace App\Models;

use CodeIgniter\Model;

class SiswaModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'siswa';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['nis', 'token', 'nama', 'kelas', 'admin_id', 'arsip'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	public function ambil($kondisi = false)
	{
		if (!$kondisi) {
			return $this->findAll();
		} else {
			return $this->where($kondisi)->first();
		}
	}

	public function isi($data)
	{
		$this->insert($data);
	}

	public function ubah($data, $dimana)
	{
		$this->set($data);
		$this->where($dimana);
		$this->update();
	}

	public function ambilKondisi($kondisi)
	{
		$this->where($kondisi);
		return $this->findAll();
	}

	public function hapus($dimana)
	{
		$this->delete($dimana);
	}

	public function ambilGrup($grup, $kondisi)
	{
		$this->where($kondisi);
		$this->groupBy($grup);
		return $this->findAll();
	}
}
