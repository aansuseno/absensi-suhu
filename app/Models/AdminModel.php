<?php

namespace App\Models;
use CodeIgniter\Model;

class AdminModel extends Model {
	protected $table = 'admin';
	protected $allowedFields = ['username', 'password', 'nama', 'no_wa', 'role', 'aktif', 'arsip'];
	protected $useTimestamps = true;

	public function ambil($kondisi = false)
	{
		if (!$kondisi) {
			return $this->findAll();
		} else {
			return $this->where($kondisi)->first();
		}
	}

	public function isi($data)
	{
		$this->insert($data);
	}

	public function ubah($data, $dimana)
	{
		$this->set($data);
		$this->where($dimana);
		$this->update();
	}

	public function ambilKondisi($kondisi)
	{
		$this->where($kondisi);
		return $this->findAll();
	}

	public function hapus($dimana)
	{
		$this->delete($dimana);
	}
}