<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/login', 'Login::index', ['filter' => 'auth']);
$routes->get('/daftar', 'Home::register', ['filter' => 'auth']);
$routes->post('/daftarProses', 'Home::registerProses', ['filter' => 'auth']);
$routes->get('/berhasil-daftar', 'Home::registerBerhasil', ['filter' => 'auth']);
$routes->get('/edit-daftar', 'Home::registerEdit', ['filter' => 'auth']);
$routes->post('/simpan-edit-daftar', 'Home::registerEditProses', ['filter' => 'auth']);

// routes untuk pengguna root
$routes->get('/root', 'Root::index', ['filter' => 'root']);
$routes->get('/root/logout', 'Root::logout', ['filter' => 'root']);
$routes->get('/root/help', 'Root::panduan', ['filter' => 'root']);
$routes->get('/root/profil', 'Root::editProfil', ['filter' => 'root']);
$routes->post('/root/edit-profil-proses', 'Root::editDataProses', ['filter' => 'root']);
$routes->post('/root/edit-pw', 'Root::editPwProses', ['filter' => 'root']);
$routes->get('/root/user-pradaftar', 'Root::petugasPradaftar', ['filter' => 'root']);
$routes->post('/root/hapus-user', 'Root::petugasHapus', ['filter' => 'root']);
$routes->post('/root/aktifkan-user', 'Root::petugasAktifkan', ['filter' => 'root']);
$routes->post('/root/arsipkan-user', 'Root::petugasArsipkan', ['filter' => 'root']);
$routes->post('/root/reset-pw-user', 'Root::petugasReset', ['filter' => 'root']);
$routes->get('/root/user-arsip', 'Root::petugasArsip', ['filter' => 'root']);
$routes->post('/root/buka-arsip-user', 'Root::petugasBukaArsip', ['filter' => 'root']);


// routes untuk admin
$routes->get('/admin', 'Admin::index', ['filter' => 'admin']);
$routes->get('/admin/keluar', 'Admin::logout', ['filter' => 'admin']);
$routes->get('/admin/petugas-pradaftar', 'Admin::petugasPradaftar', ['fiter' => 'admin']);
$routes->get('/admin/petugas-aktif', 'Admin::petugasAktif', ['fiter' => 'admin']);
$routes->get('/admin/petugas-arsip', 'Admin::petugasArsip', ['fiter' => 'admin']);
$routes->post('/admin/petugas-arsipkan', 'Admin::petugasArsipkan', ['fiter' => 'admin']);
$routes->post('/admin/petugas-hapus', 'Admin::petugasHapus', ['fiter' => 'admin']);
$routes->post('/admin/petugas-aktifkan', 'Admin::petugasAktifkan', ['fiter' => 'admin']);
$routes->post('/admin/petugas-buka-arsip', 'Admin::petugasBukaArsip', ['fiter' => 'admin']);
$routes->get('/admin/edit-profil', 'Admin::editProfil', ['filter' => 'admin']);
$routes->get('/admin/edit-password', 'Admin::editPassword', ['filter' => 'admin']);
$routes->post('/admin/edit-profil-proses', 'Admin::editProfilProses', ['fiter' => 'admin']);
$routes->post('/admin/edit-password-proses', 'Admin::editPasswordProses', ['fiter' => 'admin']);
$routes->get('/admin/siswa', 'Admin::siswa', ['filter' => 'admin']);
$routes->get('/admin/siswa/tambah', 'Admin::siswaTambah', ['filter' => 'admin']);
$routes->get('/admin/siswa/import', 'Admin::siswaImport', ['filter' => 'admin']);
$routes->post('/admin/siswa/import/proses', 'Admin::siswaImportProses', ['filter' => 'admin']);
$routes->get('/admin/kelas/(:any)', 'Admin::siswaPerkelas/$1', ['filter' => 'admin']);
$routes->post('/admin/siswa/arsipkan', 'Admin::siswaArsipkan', ['filter' => 'admin']);
$routes->post('/admin/siswa/hapus', 'Admin::siswaHapus', ['filter' => 'admin']);
$routes->get('/admin/siswa/edit/(:any)', 'Admin::siswaEdit/$1', ['filter' => 'admin']);
$routes->post('/admin/siswa/edit/proses', 'Admin::siswaEditProses', ['filter' => 'admin']);
$routes->post('/admin/siswa/kelas/edit', 'Admin::siswaEditKelas', ['filter' => 'admin']);
$routes->post('/admin/siswa/tambah', 'Admin::siswaTambahProses', ['filter' => 'admin']);
$routes->get('/admin/siswa/cari', 'Admin::siswaCari', ['filter' => 'admin']);
$routes->post('/admin/siswa/cari', 'Admin::siswaCari', ['filter' => 'admin']);
$routes->get('/admin/siswa/suhu/(:num)', 'Admin::siswaSuhu/$1', ['filter' => 'admin']);
$routes->post('/admin/siswa/hapus/record', 'Admin::siswaHapusRecord', ['filter' => 'admin']);
$routes->post('/admin/siswa/print/record', 'Admin::siswaPrintRecord', ['filter' => 'admin']);

// routes petugas
$routes->get('/petugas', 'Petugas::index', ['filter' => 'petugas']);
$routes->get('/petugas/scan/manual', 'Petugas::scanManual', ['filter' => 'petugas']);
$routes->get('/petugas/hari-ini', 'Petugas::scanHariIni', ['filter' => 'petugas']);
$routes->post('/petugas/scan/manual', 'Petugas::scanManualProses', ['filter' => 'petugas']);
$routes->post('/petugas/hari-ini', 'Petugas::scanHariIniEdit', ['filter' => 'petugas']);
$routes->get('/petugas/profil', 'Petugas::editProfil', ['filter' => 'petugas']);
$routes->post('/petugas/profil', 'Petugas::editProfilProses', ['filter' => 'petugas']);


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
