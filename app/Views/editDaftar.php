<!DOCTYPE html>
<html lang="in">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Instalasi aplikasi absen</title>

	<link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
	<div class="container-sm mt-5">
		<?php if (session()->getFlashdata('pesan') != null) { ?>
			<div class="alert alert-danger"><?= session()->getFlashdata('pesan') ?></div>
		<?php } ?>
		<?php $oke = false; if (session()->getFlashdata('lama') != null) {
			$oke = true;
		}?>
		<form action="/simpan-edit-daftar" method="post">
			<?= csrf_field(); ?>
			<h3>Pendaftaran Admin & Petugas</h3>
			<input
			 type="hidden"
			 name="id"
			 value="<?= $user['id'] ?>">
			<div class="mb-3 row">
				<label for="nama" class="col-form-label col-sm-2">Nama</label>
				<div class="col-sm-10">
					<input
					 type="text"
					 value="<?= ($oke) ? session()->getFlashdata('lama')['nama'] : $user['nama'] ?>"
					 name="nama"
					 class="form-control"
					 autofocus
					 autocomplete="off"
					 placeholder="nama"
					 required id="nama">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="username" class="col-form-label col-sm-2">Username</label>
				<div class="col-sm-10">
					<input
					 type="text"
					 name="username"
					 placeholder="username"
					 title="username digunakan untuk login, minimal diisi 8 karakter"
					 required
					 class="form-control <?= ($oke) ? 'is-invalid' : '' ?>"
					 id="username"
					 autocomplete="off"
					 minlength="8"
					 value="<?= ($oke) ? session()->getFlashdata('lama')['username'] : $user['username'] ?>">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="no_wa" class="col-sm-2 col-form-label">No. WA</label>
				<div class="col-sm-10">
					<input
					 type="text"
					 placeholder="no whatsapp"
					 required
					 title="wajib diisi untuk keperluan kelola petugas dan admin"
					 autocomplete="off"
					 name="no_wa"
					 numeric
					 class="form-control"
					 id="no_wa"
					 value="<?= ($oke) ? session()->getFlashdata('lama')['no_wa'] : $user['no_wa'] ?>">
					<div class="small text-danger" id="pesan-wa"></div>
				</div>
			</div>
			<div class="mb-3 row">
				<label for="role" class="col-sm-2 col-form-label">Jabatan</label>
				<div class="col-sm-10">
					<select name="role" title="Isikan sesuai jabatan Anda supaya disetujui admin." id="role" class="form-select">
						<option value="petugas" selected>Petugas</option>
						<option
						 value="admin"
						 <?= ($oke && session()->getFlashdata('lama')['role'] == 'admin') ? 'selected' : '' ?>
						 <?= (!$oke && $user['role'] == 'admin') ? 'selected' : '' ?>
						 >Admin</option>
					</select>
				</div>
			</div>
			<div class="alert alert-warning">Password tidak bisa diganti sebelum akun aktif. Jadi silakan masukkan password lama Anda.</div>
			<div class="mb-3 row">
				<label for="password" class="col-form-label col-sm-2">Password</label>
				<div class="col-sm-10">
					<input
					 type="password"
					 name="password"
					 required
					 autocomplete="off"
					 minlength="8"
					 class="form-control"
					 id="password">
				</div>
			</div>
			<input
			 type="submit"
			 title="Klik untuk menyimpan. Jika tidak bisa diklik silakan cek bebarapa kolom."
			 value="Simpan"
			 id="btn-simpan"
			 class="btn btn-primary float-end">
		</form>
	</div>

	<script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/jquery.min.js"></script>
	<script>
		$(document).ready(function () {
			$('#no_wa').on('input', function () {
				if($('#no_wa').val().charAt(0) != 6 || $('#no_wa').val().charAt(1) != 2) {
					$('#pesan-wa').html('Nomor WA harus diawali dengan 62')
					$('#btn-simpan').prop('disabled', true)
				} else if(isNaN($('#no_wa').val())) {
					$('#pesan-wa').html('Nomor WA harus berupa angka')
					$('#btn-simpan').prop('disabled', true)
				} else {
					$('#pesan-wa').html('')
					$('#btn-simpan').prop('disabled', false)
				}
			})
		})
	</script>
</body>
</html>