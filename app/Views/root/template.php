<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $halaman ?> | ROOT</title>
	<link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/fontawesome/css/all.min.css">
</head>
<body>
	<div class="wrapper">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container-fluid">
				<a href="#" class="navbar-brand">Absensi</a>
				<button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarAtas" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarAtas">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a
							 href="/root"
							 class="nav-link <?= ($halaman == 'Admin Aktif') ? 'active' : '' ?>">
								Admin Aktif
							</a>
						</li>
						<li class="nav-item">
							<a href="/root/user-pradaftar" class="nav-link <?= ($halaman == 'User Pradaftar') ? 'active' : '' ?>">Admin Pradaftar</a>
						</li>
						<li class="nav-item">
							<a href="/root/user-arsip" class="nav-link <?= ($halaman == 'Arsip') ? 'active' : '' ?>">Admin Arsip</a>
						</li>
						<li class="nav-item">
							<a href="/root/help" class="nav-link <?= ($halaman == 'Panduan') ? 'active' : '' ?>">Panduan</a>
						</li>
						<li class="nav-item">
							<a href="/root/profil" class="nav-link <?= ($halaman == 'Profil') ? 'active' : '' ?>">Edit Profil</a>
						</li>
						<li class="nav-item">
							<a href="/root/logout" class="nav-link text-danger">Keluar</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<?= $this->renderSection('konten') ?>
		</div>
	</div>
	<script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/jquery.min.js"></script>
	<?= $this->renderSection('js') ?>
</body>
</html>