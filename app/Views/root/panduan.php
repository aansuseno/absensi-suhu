<?= $this->extend('root/template') ?>

<?= $this->section('konten') ?>
<script>
	var pop = [
		['Level Pengguna', 'Dalam aplikasi absen ini terdapat 3 level pengguna. Antara lain Root, Admin, dan Petugas. Masing-masing ada tugasnya sendiri.<br><b>Root</b> yaitu pengguna yang bertugas untuk mengelola Admin. Hanya terdapat 1 pengguna dengan level Root, yakni Anda. Root hanya bisa mengelola Admin saja. <br><b>Admin</b> yaitu pengguna yang melakukan banyak hal. Seperti menambah siswa, menambah Petugas, dan mengelola absensi. Cara menambahkan Admin ada di panduan berikutnya. <br><b>Petugas</b> yaitu pengguna yang bertugas menambahkan data absensi setiap harinya. Jadi petugas hanya bisa me-scan code QR dan memasukkan suhu siswa. Dan hanya bisa mengedit suhu siswa yang Petugas itu tambahkan.'],
		['Tambah Admin', '1. Admin harus mendaftar sendiri di <?= base_url('daftar') ?>. <br>2. Maka data Admin akan tampil dihalaman Admin Pradaftar. <br>3. Lalu Anda setujui Admin untuk mengaktifkan akun Admin.']
	]
</script>

<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/root">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page">Panduan</li>
</ol>
<h3 class="mt-4 mb-4">Panduan</h3>
<div class="card bg-light mb-2" title="Ada 3 tingkatakan pengguna dalam aplikasi ini. Root, Admin, dan Petugas. Klik untuk lebih lanjut.">
	<div class="card-body" data-bs-toggle="modal" onclick="$('#popJudul').html(pop[0][0]); $('#popIsi').html(pop[0][1])" data-bs-target="#popup" style="cursor: pointer;">
		0. Tingkatan pengguna.
	</div>
</div>
<div class="card bg-light mb-2" title="Cara menambahkan Admin baru.">
	<div class="card-body" data-bs-toggle="modal" onclick="$('#popJudul').html(pop[1][0]); $('#popIsi').html(pop[1][1])" data-bs-target="#popup" style="cursor: pointer;">
		1. Tambah Admin.
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="popJudul">Modal title</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="popIsi">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<?= $this->endSection() ?>
