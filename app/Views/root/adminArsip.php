<?= $this->extend('root/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/root">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page">Admin Arsip</li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4">Admin Arsip</h3>
<hr>
<div style="overflow: auto;">
	<table class="table table-hover" style="min-width: 600px">
		<tr>
			<th class="col-sm-1">NO.</th>
			<th>Nama</th>
			<th>Role</th>
			<th>Aksi</th>
		</tr>
		<?php $no = 1; foreach ($userArsip as $u) {?>
			<tr>
				<td><?= $no; $no++ ?></td>
				<th><?= $u['nama'] ?></th>
				<th><?= $u['role'] ?></th>
				<th>
					<button
					 class="btn btn-secondary"
					 title="Buka arsip <?= $u['role'] ?>."
					 data-bs-toggle="modal"
					 onclick="buka(<?= $u['id'] ?>, '<?= $u['nama'] ?>')"
					 data-bs-target="#popup">
						<i class="fas fa-play"></i>
					</button>
					<a
					 href="https://wa.me/<?= $u['no_wa'] ?>"
					 target="_blank"
					 title="Hubungi <?= $u['nama'] ?> melalui WhatsApp"
					 class="btn btn-dark">
						<i class="fab fa-whatsapp" aria-hidden="true"></i>
					</a>
				</th>
			</tr>
		<?php } ?>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="popJudul">PERINGATAN !!</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="popIsi">
				...
			</div>
			<div class="modal-footer" id="popFooter">
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
	function buka(id, nama) {
		$('#popIsi').html('User akan mempunyai akses untuk mengelola aplikasi lagi. <b>Anda yakin membuka arsip '+nama+'?</b>')
		$('#popFooter').html('<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button><form action="/root/buka-arsip-user" method="post"><?= csrf_field() ?><input type="hidden" name="id" value="'+id+'"><input type="submit" class="btn btn-secondary" value="Buka Arsip"></form>')
	}
</script>
<?= $this->endSection() ?>