<?= $this->extend('root/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/root">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page">Edit Profil</li>
</ol>
<h3 class="mt-4 mb-4">Edit Profil</h3>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-danger"><?= $pesan; ?></div>
<?php } ?>
<!-- data diri -->
<form action="/root/edit-profil-proses" method="post">
	<?= csrf_field(); ?>
	<h3>Data user root</h3>
	<input type="hidden" value="<?= $root['id'] ?>" name="id">
	<input type="hidden" value="<?= $root['username'] ?>" name="username_lama">
	<div class="mb-3 row">
		<label for="nama" class="col-form-label col-sm-2">Nama</label>
		<div class="col-sm-10">
			<input type="text" value="<?= $root['nama'] ?>" name="nama" class="form-control" autofocus autocomplete="off" placeholder="nama" required id="nama">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="username" class="col-form-label col-sm-2">Username</label>
		<div class="col-sm-10">
			<input type="text" value="<?= $root['username'] ?>" name="username" placeholder="username" title="username digunakan untuk login, minimal diisi 8 karakter" required class="form-control" id="username" autocomplete="off" minlength="8">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="no_wa" class="col-sm-2 col-form-label">No. WA</label>
		<div class="col-sm-10">
			<input type="text" value="<?= $root['no_wa'] ?>" placeholder="no whatsapp" required title="wajib diisi untuk keperluan kelola petugas dan admin" autocomplete="off" name="no_wa" numeric class="form-control" id="no_wa">
			<div class="small text-danger" id="pesan-wa"></div>
		</div>
	</div>
	<input type="submit" title="Klik untuk menyimpan data user." value="Simpan" id="btn-simpan-data" class="btn btn-primary">
</form>

<!-- password -->
<hr>
<form action="/root/edit-pw" method="post">
	<?= csrf_field() ?>
	<input type="hidden" value="<?= $root['id'] ?>" name="id">
	<h3>Edit Password</h3>
	<div class="mb-3 row">
		<label for="password" class="col-form-label col-sm-2">Password</label>
		<div class="col-sm-10">
			<input type="password" name="password" required autocomplete="off" minlength="8" class="form-control" id="password">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="konfpw" class="col-form-label col-sm-2">Konfirmasi</label>
		<div class="col-sm-10">
			<input type="password" required autocomplete="off" class="form-control" id="konfpw">
			<div class="small text-danger" id="pesan-pw"></div>
		</div>
	</div>
	<input type="submit" title="Klik untuk menyimpan password baru." value="Simpan" id="btn-simpan-pw" class="btn btn-primary">
</form>
<?= $this->endSection() ?>

<!-- js -->
<?= $this->section('js') ?>
<script>
	$(document).ready(function () {
		$('#konfpw').on('input', function () {
			if($('#konfpw').val() != $('#password').val()) {
				$('#pesan-pw').html('konfirmasi password salah..')
				$('#btn-simpan-pw').prop('disabled', true)
			} else {
				$('#pesan-pw').html('')
				$('#btn-simpan-pw').prop('disabled', false)
			}
		})

		$('#no_wa').on('input', function () {
			if($('#no_wa').val().charAt(0) != 6 || $('#no_wa').val().charAt(1) != 2) {
				$('#pesan-wa').html('Nomor WA harus diawali dengan 62')
				$('#btn-simpan-data').prop('disabled', true)
			} else if(isNaN($('#no_wa').val())) {
				$('#pesan-wa').html('Nomor WA harus berupa angka')
				$('#btn-simpan-data').prop('disabled', true)
			} else {
				$('#pesan-wa').html('')
				$('#btn-simpan-data').prop('disabled', false)
			}
		})
	})
</script>
<?= $this->endSection() ?>