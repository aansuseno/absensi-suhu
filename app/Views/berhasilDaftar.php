<!DOCTYPE html>
<html lang="in">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Berhasil mendaftar <?= $user['role'] ?></title>

	<link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
	<div class="container-sm mt-5">
		<div class="card">
			<div class="card-header">Berhasil Mendaftar <?= $user['role'] ?></div>
			<div class="card-body">
				Silakan tunggu konfirmasi dari user <i>Root</i> yang akan dikirim ke <?= $user['no_wa'] ?>. Atau bisa menghubungi <a href="https://wa.me/<?= $root['no_wa'] ?>" target="_blank" title="Nomor WhatsApp <?= $root['nama'] ?>"><?= $root['no_wa'] ?></a>.
				<p><a href="/edit-daftar">Edit jawaban Anda di sini.</a> Sebelum halaman ini terhapus.</p>
			</div>
		</div>
	</div>

	<script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/jquery.min.js"></script>
</body>
</html>