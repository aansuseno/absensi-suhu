<?= $this->extend('petugas/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item active" aria-current="page">Home</li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>

<style>
    hr {
        margin-top: 32px;
    }
    input[type="file"] {
        display: block;
        margin-bottom: 16px;
    }
    div {
        margin-bottom: 16px;
    }
    #flash-toggle {
        display: none;
    }

	canvas {
		width: 100% !important;
		max-width: 500px !important;
	}
</style>
<div>
    <video id="qr-video" style="opacity: 0; position: absolute; z-index: -5;">	</video>
    <br>
	<button id="show-scan-region" class="btn btn-primary btn-lg col-12" type="submit">MULAI</button>
</div>
<br>
<div>
    <b>Pilih Kamera:</b>
    <select id="cam-list" class="form-control">
        <option value="environment" selected>Environment Facing (default)</option>
        <option value="user">User Facing</option>
    </select>
</div>
<div>
    <button id="flash-toggle">📸 Flash: <span id="flash-state">off</span></button>
</div>
<br>
<div id="input-suhu-block" style="position: absolute; height: 100vh; width: 100vw; top: 0; left: -100vw;" class="bg-light">
	
</div>
<button id="stop-button" class="btn btn-danger">Jeda</button>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="node_modules/qr-scanner/qr-scanner.umd.min.js"></script>
<script>
	// const qrScanner = new QrScanner($('#qr-kode'), result => console.log('decoded qr code:', result));
</script>
<script type="module">
    import QrScanner from "/node_modules/qr-scanner/qr-scanner.min.js";
    QrScanner.WORKER_PATH = '/node_modules/qr-scanner/qr-scanner-worker.min.js';
    const video = document.getElementById('qr-video');
    const camList = document.getElementById('cam-list');
    const flashToggle = document.getElementById('flash-toggle');
    const flashState = document.getElementById('flash-state');

    function setResult(hasil) {
		$.ajax({url: '/petugas/scanProses?token='+hasil,
		success: function (result) {
			$('#input-suhu-block').html(result)
		}})
		$('#input-suhu-block').animate({
			'left': '0'
		}, 500)
		scanner.stop()
		$('canvas').css({
			'display': 'none'
		})
    }

    // ####### Web Cam Scanning #######

    const scanner = new QrScanner(video, result => setResult(result))

    const updateFlashAvailability = () => {
        scanner.hasFlash().then(hasFlash => {
            flashToggle.style.display = hasFlash ? 'inline-block' : 'none';
        });
    };

    // for debugging
    window.scanner = scanner;

    $('#show-scan-region').on('click', function (e) {
		mulai()
        const input = e.target;
        const label = input.parentNode;
        label.parentNode.insertBefore(scanner.$canvas, label.nextSibling);
        scanner.$canvas.style.display =  'block';
    });

    camList.addEventListener('change', event => {
        scanner.setCamera(event.target.value).then(updateFlashAvailability);
    });

    flashToggle.addEventListener('click', () => {
        scanner.toggleFlash().then(() => flashState.textContent = scanner.isFlashOn() ? 'on' : 'off');
    });

    function mulai() {
        scanner.start().then(() => {
			updateFlashAvailability();
			QrScanner.listCameras(true).then(cameras => cameras.forEach(camera => {
				const option = document.createElement('option');
				option.value = camera.id;
				option.text = camera.label;
				camList.add(option);
			}));
		});
    }

    document.getElementById('stop-button').addEventListener('click', () => {
        scanner.stop();
    });
</script>
<?= $this->endSection() ?>