<?= $this->extend('petugas/template') ?>

<?= $this->section('konten') ?>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning mt-3"><?= $pesan; ?></div>
<?php } ?>
<?php $oke = false; if (session()->getFlashdata('lama') != null) {
	$oke = true;
}?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<form action="/petugas/scan/manual" method="post" style="max-width: 500px; margin: auto; margin-top: 50px;">
	<?= csrf_field() ?>
	<label for="nis">NIS :</label>
	<input
	 type="number"
	 name="nis"
	 autocomplete="off"
	 required
	 autofocus
	 id="nis"
	 placeholder="12345"
	 value="<?= ($oke) ? session()->getFlashdata('lama')['nis'] : '' ?>"
	 class="form-control mb-3">
	<label for="suhu">Suhu :</label>
	<input
	 type="number"
	 name="suhu"
	 autocomplete="off"
	 required
	 id="suhu"
	 placeholder="34.56"
	 step="0.01"
	 value="<?= ($oke) ? session()->getFlashdata('lama')['suhu'] : '' ?>"
	 class="form-control mb-3">
	<input type="submit" value="Kirim" class="btn btn-primary col-12 mb-5">
	<div class="alert alert-success">
		<h5>Aturan penulisan suhu</h5>
		<ol>
			<li>Maksimal jumlah angka di belakang koma yaitu dua angka.</li>
			<li>Koma dalam penulisan desimal menggunakan aturan internasional, yaitu menggunakan tanda titik '<b>.</b>'.</li>
		</ol>
	</div>
</form>
<?= $this->endSection() ?>