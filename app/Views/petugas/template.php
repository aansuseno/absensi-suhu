<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $halaman ?> | PETUGAS</title>
	<link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/fontawesome/css/all.min.css">
</head>
<body>
	<div class="wrapper">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container-fluid">
				<a href="#" class="navbar-brand">Absensi</a>
				<button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarAtas" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarAtas">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a
							 href="/petugas"
							 class="nav-link <?= ($halaman == 'Pindai Suhu') ? 'active' : '' ?>">
								Scan
							</a>
						</li>
						<li class="nav-item">
							<a
							 href="/petugas/scan/manual"
							 class="nav-link <?= ($halaman == 'Pindai Suhu Manual') ? 'active' : '' ?>">
								Scan Manual
							</a>
						</li>
						<li class="nav-item">
							<a
							 href="/petugas/hari-ini"
							 class="nav-link <?= ($halaman == 'Scan Hari Ini') ? 'active' : '' ?>">
								Hari Ini
							</a>
						</li>
						<li class="nav-item dropdown">
							<a
							 class="nav-link dropdown-toggle <?= ($halaman == 'Edit Profil') ? 'active' : '' ?>"
							 href="#"
							 id="navbarDropdownMenuLink"
							 role="button"
							 data-bs-toggle="dropdown"
							 aria-expanded="false">
								Pengaturan
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="/petugas/profil">Edit Profil</a>
								<a class="dropdown-item text-danger" href="/petugas/logout">Keluar</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<?= $this->renderSection('konten') ?>
		</div>
	</div>
	<script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/jquery.min.js"></script>
	<?= $this->renderSection('js') ?>
</body>
</html>