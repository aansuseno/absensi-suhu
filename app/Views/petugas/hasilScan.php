<div style="max-width: 700px; margin: auto; padding: 20px;">
	<table class="table table-borderless">
		<tr>
			<td>Nama : </td>
			<th id="nama-siswa"><?= $nama ?></th>
		</tr>
		<tr>
			<td>Kelas : </td>
			<th id="kelas-siswa"><?= $kelas ?></th>
		</tr>
		<tr>
			<td>NIS : </td>
			<th id="nis-siswa"><?=  $nis ?></th>
		</tr>
	</table>
	<input type="hidden" id="id-siswa" value="<?= $id ?>">
	<label for="suhu-siswa" class="mt-5 ">Suhu :</label>
	<input type="number" step="0.01" placeholder="35.67" autocomplete="off" class="form-control col-12 mb-3" id="suhu-siswa">
	<button class="btn btn-primary col-12" id="tombol-kirim-suhu" onclick="
	if($('#suhu-siswa').val() != '' && !isNaN($('#suhu-siswa').val())) {
		$('#tombol-kirim-suhu').prop('disabled', true)
		$.ajax({url: '/petugas/scanKirimProses?id='+$('#id-siswa').val()+'&suhu='+$('#suhu-siswa').val(),
		success: function (result) {
			$('#input-suhu-block').animate({
				'left': '-100vw'
			}, 500)
			$('#input-suhu-block').html('proses...')
		}})
	}
	">Kirim</button>
</div>