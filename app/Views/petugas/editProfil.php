<?= $this->extend('petugas/template') ?>

<?= $this->section('konten') ?>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning mt-3"><?= $pesan; ?></div>
<?php } ?>
<?php $oke = false; if (session()->getFlashdata('lama') != null) {
	$oke = true;
}?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<form action="/petugas/profil" method="post">
	<?= csrf_field() ?>
	<div class="mb-3 row">
		<label for="nama" class="col-form-label col-sm-2">Nama</label>
		<div class="col-sm-10">
			<input
				type="text"
				value="<?= ($oke) ? session()->getFlashdata('lama')['nama'] : $dataAdmin['nama'] ?>"
				name="nama"
				class="form-control"
				autofocus
				autocomplete="off"
				placeholder="nama"
				required id="nama">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="username" class="col-form-label col-sm-2">Username</label>
		<div class="col-sm-10">
			<input
				type="text"
				name="username"
				placeholder="username"
				title="username digunakan untuk login, minimal diisi 8 karakter"
				required
				class="form-control <?= ($oke) ? 'is-invalid' : '' ?>"
				id="username"
				autocomplete="off"
				minlength="8"
				value="<?= ($oke) ? session()->getFlashdata('lama')['username'] : $dataAdmin['username'] ?>">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="no_wa" class="col-sm-2 col-form-label">No. WA</label>
		<div class="col-sm-10">
			<input
				type="text"
				placeholder="no whatsapp"
				required
				title="wajib diisi untuk keperluan kelola petugas dan admin"
				autocomplete="off"
				name="no_wa"
				numeric
				class="form-control"
				id="no_wa"
				value="<?= ($oke) ? session()->getFlashdata('lama')['no_wa'] : $dataAdmin['no_wa'] ?>">
			<div class="small text-danger" id="pesan-wa"></div>
		</div>
	</div>
	<input
	 type="submit"
	 title="Klik untuk menyimpan. Jika tidak bisa diklik silakan cek bebarapa kolom."
	 value="Simpan"
	 id="btn-simpan"
	 class="btn btn-primary float-end">
</form>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
	$(document).ready(function () {
		$('#no_wa').on('input', function () {
			if($('#no_wa').val().charAt(0) != 6 || $('#no_wa').val().charAt(1) != 2) {
				$('#pesan-wa').html('Nomor WA harus diawali dengan 62')
				$('#btn-simpan').prop('disabled', true)
			} else if(isNaN($('#no_wa').val())) {
				$('#pesan-wa').html('Nomor WA harus berupa angka')
				$('#btn-simpan').prop('disabled', true)
			} else {
				$('#pesan-wa').html('')
				$('#btn-simpan').prop('disabled', false)
			}
		})
	})
</script>
<?= $this->endSection() ?>