<?= $this->extend('petugas/template') ?>

<?= $this->section('konten') ?>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<div style="overflow: auto;">
	<table class="table table-hover" style="min-width: 600px">
		<tr>
			<th class="col-sm-1">NO.</th>
			<th>NIS</th>
			<th>Nama</th>
			<th>Suhu</th>
			<th>Aksi</th>
		</tr>
		<?php $no = 1; foreach ($record as $u) {?>
			<tr>
				<td><?= $no; $no++ ?></td>
				<td><?= $u['nis'] ?></td>
				<td><?= $u['nama'] ?></td>
				<td><?= $u['suhu'] ?></td>
				<td>
					<button
					 class="btn btn-warning"
					 title="Edit suhu."
					 data-bs-toggle="modal"
					 onclick="edit(<?= $u['id'] ?>, '<?= $u['suhu'] ?>')"
					 data-bs-target="#popup">
						<i class="fas fa-edit"></i>
					</button>
				</td>
			</tr>
		<?php } ?>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Suhu !!</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="popIsi">
			</div>
			<div class="modal-footer" id="popFooter">
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
	function edit(id, suhu) {
		$('#popIsi').html('<form action="/petugas/hari-ini" method="post"><?= csrf_field() ?><input type="hidden" name="id" value="'+id+'"><label for="suhu">Suhu</label><input type="number" name="suhu" autocomplete="off" required	id="suhu" placeholder="34.56" step="0.01" value="'+suhu+'" class="form-control mb-3"><input type="submit" value="Edit" class="btn btn-primary col-12 mb-5"></form>')
		$('#popFooter').html('<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>')
	}
</script>
<?= $this->endSection() ?>