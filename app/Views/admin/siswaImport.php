<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<form action="/admin/siswa/import/proses" method="post" enctype="multipart/form-data">
	<div class="mb-3">
		<label for="data_excel" class="form-label"></label>
		<input type="file" required name="data_excel" accept=".xls, .xlsx" class="form-control" id="data_excel">
	</div>
	<input type="submit" value="Upload" class="btn btn-primary">
</form>
<div class="alert alert-success mt-5">
	<h5>ATURAN</h5>
	<ol>
		<li>Data siswa dimulai dari kolom dan baris pertama. Tidak perlu header.</li>
		<li>Kolom pertama(<b>A</b>) berisi NIS siswa.</li>
		<li>Kolom kedua(<b>B</b>) berisis nama siswa.</li>
		<li>Kolom ketiga(<b>C</b>) berisi kelas siswa(sebaiknya berupa singkatan: xii rpl).</li>
		<li>File harus berupa <i>.xls</i> atau <i>.xlsx</i>.</li>
		<li>NIS, nama, dan kelas harus diisi.</li>
		<li>NIS harus berupa bilangan bulat.</li>
	</ol>
</div>
<?= $this->endSection() ?>