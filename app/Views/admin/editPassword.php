<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<form action="/admin/edit-password-proses" method="post">
	<?= csrf_field() ?>
	<div class="mb-3 row">
		<label for="password" class="col-form-label col-sm-2">Password Baru</label>
		<div class="col-sm-10">
			<input
				type="password"
				name="password"
				required
				autocomplete="off"
				minlength="8"
				class="form-control"
				id="password">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="konfpw" class="col-form-label col-sm-2">Konfirmasi</label>
		<div class="col-sm-10">
			<input
				type="password"
				required
				autocomplete="off"
				class="form-control"
				id="konfpw">
			<div class="small text-danger" id="pesan-pw"></div>
		</div>
	</div>
	<input
	 type="submit"
	 title="Klik untuk menyimpan. Jika tidak bisa diklik silakan cek bebarapa kolom."
	 value="Simpan"
	 id="btn-simpan"
	 disabled="true"
	 class="btn btn-primary float-end">
</form>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
	$(document).ready(function () {
		$('#konfpw').on('input', function () {
			if($('#konfpw').val() != $('#password').val()) {
				$('#pesan-pw').html('konfirmasi password salah..')
				$('#btn-simpan').prop('disabled', true)
			} else {
				$('#pesan-pw').html('')
				$('#btn-simpan').prop('disabled', false)
			}
		})
	})
</script>
<?= $this->endSection() ?>