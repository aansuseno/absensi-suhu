<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $halaman ?> | ADMIN</title>
	<link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/fontawesome/css/all.min.css">
	<?= $this->renderSection('jsAtas') ?>
</head>
<body>
	<div class="wrapper">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container-fluid">
				<a href="#" class="navbar-brand">Absensi</a>
				<button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarAtas" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarAtas">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a
							 href="/admin"
							 class="nav-link <?= ($halaman == 'Beranda') ? 'active' : '' ?>">
								Beranda
							</a>
						</li>
						<li class="nav-item dropdown">
							<a
							 class="nav-link dropdown-toggle <?= ($halaman == 'Import Siswa' || $halaman == 'Tampil Kelas' || $halaman == 'Kelas' || $halaman == 'Edit Siswa' || $halaman == 'Tambah Siswa' || $halaman == 'Suhu Siswa') ? 'active' : '' ?>"
							 href="#"
							 id="navbarDropdownMenuLink"
							 role="button"
							 data-bs-toggle="dropdown"
							 aria-expanded="false">
								Siswa
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="/admin/siswa">Semua Siswa</a>
								<a class="dropdown-item" href="/admin/siswa/tambah">Tambah Siswa</a>
								<a class="dropdown-item" href="/admin/siswa/import">Import Siswa</a>
								<a class="dropdown-item" href="/admin/siswa/cari">Cari Siswa</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a
							 class="nav-link dropdown-toggle <?= ($halaman == 'Petugas Pradaftar' || $halaman == 'Petugas Aktif' || $halaman == 'Petugas Arsip') ? 'active' : '' ?>"
							 href="#"
							 id="dropdown-petugas"
							 role="button"
							 data-bs-toggle="dropdown"
							 aria-expanded="false">
								Petugas
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-petugas">
								<a class="dropdown-item" href="/admin/petugas-aktif">Petugas Aktif</a>
								<a class="dropdown-item" href="/admin/petugas-pradaftar">Petugas Pradaftar</a>
								<a class="dropdown-item" href="/admin/petugas-arsip">Petugas Arsip</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a
							 class="nav-link dropdown-toggle <?= ($halaman == 'Edit Profil' || $halaman == 'Edit Password') ? 'active' : '' ?>"
							 href="#"
							 id="dropdown-profil"
							 role="button"
							 data-bs-toggle="dropdown"
							 aria-expanded="false">
								Pengaturan
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-profil">
								<a class="dropdown-item" href="/admin/edit-profil">Edit Profil</a>
								<a class="dropdown-item" href="/admin/edit-password">Edit Password</a>
								<a class="dropdown-item text-danger" href="/admin/keluar">Keluar</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<?= $this->renderSection('konten') ?>
		</div>
	</div>
	<script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/jquery.min.js"></script>
	<?= $this->renderSection('js') ?>
</body>
</html>