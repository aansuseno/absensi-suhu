<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<?php $oke = false; if (session()->getFlashdata('lama') != null) {
	$oke = true;
}?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<form action="/admin/siswa/tambah" method="post">
	<?= csrf_field() ?>
	<div class="mb-3 row">
		<label for="nama" class="col-form-label col-sm-2">Nama</label>
		<div class="col-sm-10">
			<input
				type="text"
				name="nama"
				required
				autofocus
				autocomplete="off"
				class="form-control"
				value="<?= ($oke) ? session()->getFlashdata('lama')['nama'] : '' ?>"
				id="nama">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="nis" class="col-form-label col-sm-2">NIS</label>
		<div class="col-sm-10">
			<input
				type="number"
				name="nis"
				required
				autocomplete="off"
				class="form-control"
				value="<?= ($oke) ? session()->getFlashdata('lama')['nis'] : '' ?>"
				id="nis">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="kelas" class="col-form-label col-sm-2">Kelas</label>
		<div class="col-sm-10">
			<input
				type="text"
				name="kelas"
				required
				autocomplete="off"
				class="form-control"
				value="<?= ($oke) ? session()->getFlashdata('lama')['kelas'] : '' ?>"
				id="kelas">
		</div>
	</div>
	<input
	 type="submit"
	 title="Klik untuk menyimpan. Jika tidak bisa diklik silakan cek bebarapa kolom."
	 value="Simpan"
	 class="btn btn-primary float-end">
</form>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
</script>
<?= $this->endSection() ?>