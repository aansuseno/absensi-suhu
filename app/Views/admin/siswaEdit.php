<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item"><a href="/admin/siswa">Kelas</a></li>
	<li class="breadcrumb-item"><a href="/admin/kelas/<?= $siswa['kelas'] ?>"><?= $siswa['kelas'] ?></a></li>
	<li class="breadcrumb-item active" aria-current="page">Edit Siswa<?= $siswa['nama'] ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<?php $oke = false; if (session()->getFlashdata('lama') != null) {
	$oke = true;
}?>
<h3 class="mt-4"><?= $halaman.' '.$siswa['nama'] ?></h3>
<hr>
<form action="/admin/siswa/edit/proses" method="post">
	<?= csrf_field() ?>
	<input type="hidden" name="id" value="<?= $siswa['id'] ?>">
	<div class="mb-3 row">
		<label for="nama" class="col-form-label col-sm-2">Nama</label>
		<div class="col-sm-10">
			<input
				type="text"
				value="<?= ($oke) ? session()->getFlashdata('lama')['nama'] : $siswa['nama'] ?>"
				name="nama"
				class="form-control"
				autofocus
				autocomplete="off"
				placeholder="nama"
				required id="nama">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="nis" class="col-form-label col-sm-2">NIS</label>
		<div class="col-sm-10">
			<input
				type="number"
				value="<?= ($oke) ? session()->getFlashdata('lama')['nis'] : $siswa['nis'] ?>"
				name="nis"
				class="form-control"
				autocomplete="off"
				placeholder="nis"
				required
				id="nis">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="kelas" class="col-form-label col-sm-2">Kelas</label>
		<div class="col-sm-10">
			<input
				type="text"
				value="<?= ($oke) ? session()->getFlashdata('lama')['kelas'] : $siswa['kelas'] ?>"
				name="kelas"
				class="form-control"
				autocomplete="off"
				placeholder="kelas"
				required
				id="kelas">
		</div>
	</div>
	<input
	 type="submit"
	 title="Klik untuk menyimpan. Jika tidak bisa diklik silakan cek bebarapa kolom."
	 value="Simpan"
	 id="btn-simpan"
	 class="btn btn-primary float-end">
</form>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
</script>
<?= $this->endSection() ?>