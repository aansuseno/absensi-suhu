<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item"><a href="/admin/siswa">Kelas</a></li>
	<li class="breadcrumb-item"><a href="/admin/kelas/<?= $siswa['kelas'] ?>"><?= $siswa['kelas'] ?></a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman.' '.$siswa['nama'] ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<table class="table table-hover">
	<tr>
		<th>No.</th>
		<th>Tanggal</th>
		<th>Suhu(C)</th>
		<th>Aksi</th>
	</tr>
	<?php $no = 1; foreach($record as $r) { ?>
		<tr>
			<td><?= $no++ ?></td>
			<td><?php echo date('Y, M d -- H:m', strtotime($r['created_at'])) ?></td>
			<td><?= $r['suhu'] ?></td>
			<td>
			<button
				class="btn btn-danger"
				title="Hapus <?= $siswa['nama'] ?>."
				data-bs-toggle="modal"
				onclick="hapus(<?= $r['id'] ?>, '<?= $siswa['nama'] ?>', '<?= $siswa['nis'] ?>')"
				data-bs-target="#popup">
				<i class="fas fa-trash"></i>
			</button>
			</td>
		</tr>
	<?php } ?>
</table>

<!-- Modal -->
<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">PERINGATAN !!</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="popIsi" style="overflow: auto">
			</div>
			<div class="modal-footer" id="popFooter">
			<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
				<button id="donlod" class="btn btn-success" title="Download kode QR"><i class="fas fa-download"></i></button>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
	function hapus(id, nama, nis) {
		$('#popIsi').html(nama+' akan dihapus secara permanen, disarankan untuk diarsipkan saja karena data record akan hilang. <b>Anda yakin ingin menghapus '+nama+'?</b>')
		$('#popFooter').html('<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button><form action="/admin/siswa/hapus/record" method="post"><?= csrf_field() ?><input type="hidden" name="id" value="'+id+'"><input type="hidden" name="nis" value="'+nis+'"><input type="submit" class="btn btn-danger" value="Hapus"></form>')
	}
</script>
<?= $this->endSection() ?>