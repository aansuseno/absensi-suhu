<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item"><a href="/admin/siswa">Kelas</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman.' '.$kelas ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman.' '.$kelas ?></h3>
<hr>
<div style="overflow: auto;">
	<button
	 class="btn btn-success"
	 title="Naik Kelas."
	 data-bs-toggle="modal"
	 onclick="naikKelas('<?= $kelas ?>')"
	 data-bs-target="#popup">
		Edit Kelas
	</button>
	<button
	 class="btn btn-dark"
	 title="Print record satu kelas."
	 data-bs-toggle="modal"
	 onclick="record('<?= $kelas ?>')"
	 data-bs-target="#popup">
		Print Excel
	</button>
	<table class="table table-hover" style="min-width: 600px">
		<tr>
			<th class="col-sm-1">NO.</th>
			<th>NIS</th>
			<th>Nama</th>
			<th>Aksi</th>
		</tr>
		<?php $no = 1; foreach ($siswa as $u) {?>
			<tr>
				<td><?= $no; $no++ ?></td>
				<td><?= $u['nis'] ?></td>
				<td><?= $u['nama'] ?></td>
				<th>
					<a
					 class="btn btn-dark"
					 title="List suhu kehadiran <?= $u['nama'] ?>."
					 href="/admin/siswa/suhu/<?= $u['nis'] ?>">
						<i class="fas fa-list"></i>
					</a>
					<a
					 class="btn btn-warning"
					 title="Edit data <?= $u['nama'] ?>."
					 href="/admin/siswa/edit/<?= $u['nis'] ?>">
						<i class="fas fa-edit"></i>
					</a>
					<button
					 class="btn btn-success"
					 title="Print Token <?= $u['nama'] ?>."
					 data-bs-toggle="modal"
					 onclick="qr(<?= $u['id'] ?>, '<?= $u['nama'] ?>', '<?= $u['token'] ?>', '<?= $u['kelas'] ?>', <?= $u['nis'] ?>)"
					 data-bs-target="#popup">
						<i class="fas fa-qrcode"></i>
					</button>
					<button
					 class="btn btn-secondary"
					 title="Arsipkan <?= $u['nama'] ?>."
					 data-bs-toggle="modal"
					 onclick="arsip(<?= $u['id'] ?>, '<?= $u['nama'] ?>', '<?= $u['kelas'] ?>')"
					 data-bs-target="#popup">
						<i class="fas fa-archive"></i>
					</button>
					<button
					 class="btn btn-danger"
					 title="Hapus <?= $u['nama'] ?>."
					 data-bs-toggle="modal"
					 onclick="hapus(<?= $u['id'] ?>, '<?= $u['nama'] ?>', '<?= $u['kelas'] ?>')"
					 data-bs-target="#popup">
						<i class="fas fa-trash"></i>
					</button>
				</th>
			</tr>
		<?php } ?>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">PERINGATAN !!</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="popIsi" style="overflow: auto">
			</div>
			<div class="modal-footer" id="popFooter">
			<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
				<button id="donlod" class="btn btn-success" title="Download kode QR"><i class="fas fa-download"></i></button>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('jsAtas') ?>
<script>
	
</script>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="/qrcode/qrcode.min.js"></script>
<script src="/node_modules/dom-to-image/dist/dom-to-image.min.js"></script>
<script src="/node_modules/file-saver/dist/FileSaver.min.js"></script>
<script>
	namaPublic =''
	kelasPublic=''

	function arsip(id, nama, kelas) {
		$('#popIsi').html('Siswa tidak akan muncul lagi diaplikasi, tapi masih ada di dalam database. Fitur ini digunakan apabila siswa sudah tidak lagi bersekolah di sekolah yang bersangkutan. Anda bisa melihatnya di database Anda, silakan hubungi pihak sever. <b>Anda yakin ingin mengarsipkan '+nama+'?</b>')
		$('#popFooter').html('<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button><form action="/admin/siswa/arsipkan" method="post"><?= csrf_field() ?><input type="hidden" name="id" value="'+id+'"><input type="hidden" name="kelas" value="'+kelas+'"><input type="submit" class="btn btn-secondary" value="Arsipkan"></form>')
	}

	function hapus(id, nama, kelas) {
		$('#popIsi').html(nama+' akan dihapus secara permanen, disarankan untuk diarsipkan saja karena data record akan hilang. <b>Anda yakin ingin menghapus '+nama+'?</b>')
		$('#popFooter').html('<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button><form action="/admin/siswa/hapus" method="post"><?= csrf_field() ?><input type="hidden" name="id" value="'+id+'"><input type="hidden" name="kelas" value="'+kelas+'"><input type="submit" class="btn btn-danger" value="Hapus"></form>')
	}

	function qr(id, nama, token, kelas, nis) {
		$('#popIsi').html('<div id="kartu" style="width: 9.9cm; height: 6.7cm; display: flex; flex-wrap: wrap;" class="bg-light rounded"><div style="width: 100%; height: 20%; padding-top: 10px;"><center><h5>Kartu Absensi</h5></center></div><div style="width: 50%; height: 80%; padding: 10px;"><h6 id="nama-cetak"></h6><h5 id="nis-cetak"></h5></div><div style="width: 50%; height: 80%; display: flex; align-items: center; justify-content: center;"><div id="gambar-qr"></div></div></div>')
		qrcode = new QRCode(document.getElementById("gambar-qr"), {
			width : 150,
			height : 150
		})
		$('#nama-cetak').html(nama)
		$('#nis-cetak').html(nis)
		namaPublic = nama
		kelasPublic = kelas
		qrcode.makeCode(token)
	}

	$(document).ready(function () {
		$('#donlod').on('click', function () {
			domtoimage.toBlob(document.getElementById('kartu'))
			.then(function (blob) {
				window.saveAs(blob, kelasPublic+'_'+namaPublic+'.png')
			})
		})
	})

	function naikKelas(kelas) {
		$('#popIsi').html('<div class="alert alert-success">Edit nama kelas untuk semua siswa yang berada di kelas '+kelas+'.</div><form action="/admin/siswa/kelas/edit" method="post"><?= csrf_field() ?><input type="hidden" name="kelas_lama" value="'+kelas+'"><input type="text" class="form-control mb-3" required autofocus value="<?= $kelas ?>" autocomplete="off" name="kelas_baru"><input type="submit" class="btn btn-primary" value="Simpan"></form>')
		$('#popFooter').html('')
	}

	function record(kelas) {
		$('#popIsi').html('<form action="/admin/siswa/print/record" method="post"><?= csrf_field() ?><input type="hidden" name="kelas" value="'+kelas+'"><label for="tgl_dari">Dari :</label><input type="date" class="form-control mb-3" value="<?= date("Y-m-d", strtotime(date("Y-m-d")." - 30 days")) ?>" name="tgl_dari"><label for="tgl_sampai">Sampai :</label><input type="date" class="form-control mb-3" value="<?= date("Y-m-d") ?>" name="tgl_sampai"><input type="submit" class="btn btn-primary" value="Print"></form>')
		$('#popFooter').html('')
	}
</script>
<?= $this->endSection() ?>