<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<form action="/admin/siswa/cari" method="post">
	<?= csrf_field() ?>
	<div class="input-group mt-3 mb-3">
		<input
		 type="text"
		 name="cari"
		 autofocus
		 required
		 autocomplete="off"
		 aria-describedby="kolom-cari"
		 class="form-control"
		 title="Cari siswa"
		 placeholder="Cari berdasar nama atau NIS">
		<button class="input-group-text" type="submit" id="kolom-cari"><i class="fas fa-search"></i></button>
	</div>
</form>
<div style="overflow: auto;">
	<table class="table table-hover" style="min-width: 600px">
		<tr>
			<th class="col-sm-1">NO.</th>
			<th>NIS</th>
			<th>Nama</th>
			<th>Kelas</th>
		</tr>
		<?php $no = 1; foreach ($siswa as $u) {?>
			<tr>
				<td><?= $no; $no++ ?></td>
				<td><?= $u['nis'] ?></td>
				<td><?= $u['nama'] ?></td>
				<td><a href="/admin/kelas/<?= $u['kelas'] ?>"><?= $u['kelas'] ?></a></td>
			</tr>
		<?php } ?>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="/qrcode/qrcode.min.js"></script>
<script src="/node_modules/dom-to-image/dist/dom-to-image.min.js"></script>
<script src="/node_modules/file-saver/dist/FileSaver.min.js"></script>
<script>
	
</script>
<?= $this->endSection() ?>