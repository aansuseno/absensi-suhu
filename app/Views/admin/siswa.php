<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<ol class="breadcrumb mt-3">
	<li class="breadcrumb-item"><a href="/admin">Home</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?= $halaman ?></li>
</ol>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning"><?= $pesan; ?></div>
<?php } ?>
<h3 class="mt-4"><?= $halaman ?></h3>
<hr>
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
	<?php foreach ($kelas as $key => $k) {?>
	<a href="/admin/kelas/<?= $k['kelas'] ?>" class="card col-sm-3 m-2" style="cursor: pointer;" title="Klik untuk menampilkan semua siswa kelas <?= $k['kelas'] ?>">
		<div class="card-body">
			<?= $k['kelas'] ?>
		</div>
	</a>
	<?php } ?>
</div>
<?= $this->endSection() ?>