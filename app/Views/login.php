<!DOCTYPE html>
<html lang="in">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Halaman Login</title>

	<link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body class="bg-light">
	<div class="wrapper">
		<div class="container">
			<form action="/login/cek" style="max-width: 360px; margin: auto; box-shadow: 0 20px 30px rgba(0,0,0,0.2);" class="bg-white rounded p-3 mt-5" method="post">
				<h1 class="text-center mb-4">Login</h1>
				<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
					<div class="alert alert-danger"><?= $pesan; ?></div>
				<?php } ?>
				<div class="form-group">
					<label for="username">Username : </label>
					<input type="text" name="username" id="username" placeholder="Username" autofocus autocomplete="off" required class="form-control mb-3">
				</div>
				<div class="form-group">
					<label for="password">Password :</label>
					<input type="password" name="password" id="password" placeholder="Password" required class="form-control mb-3">
				</div>
				<input type="submit" value="Login" class="mb-3 btn btn-primary col-sm-12">
				<p style="text-align: right; cursor: pointer;" id="lupa-pw">Lupa password?</p>
				<div id="hub-admin" title="Klik untuk menutup." style="display: none; cursor: pointer;" class="alert alert-warning">Silakan hubungi <a href="https://wa.me/<?= $wa_admin ?>" target="_blank" rel="noopener noreferrer" title="Hubungi admin melalui WhatsApp.">admin</a> untuk me-reset password Anda.</div>
			</form>
		</div>
	</div>
	<script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="/jquery.min.js"></script>
	<script>
		$(document).ready(function () {
			$('#lupa-pw').on('click', function () {
				$('#hub-admin').show(300)
			})
			$('#hub-admin').on('click', function () {
				$('#hub-admin').hide(300)
			})
		})
	</script>
</body>
</html>