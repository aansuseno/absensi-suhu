<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Record extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id' => [
				'type' 			=> 'INT',
				'constraint'	=> 9,
				'auto_increment'=> true,
				'unsigned'		=> true,
			],
			'admin_id' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unsigned'		=> true,
			],
			'siswa_id' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unsigned'		=> true,
			],
			'suhu' => [
				'type'			=> 'FLOAT',
				'constraint'	=> 9,
				'default'		=> 30.00,
			],
			'created_at' => [
				'type' 			=> 'DATETIME',
				'null'			=> true,
			],
			'updated_at' => [
				'type' 			=> 'DATETIME',
				'null'			=> true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('admin_id', 'admin', 'id', 'CASCADE', 'CASCADE');
		$this->forge->addForeignKey('siswa_id', 'siswa', 'id', 'CASCADE', 'CASCADE');
		$this->forge->createTable('record');
    }

    public function down()
    {
		$this->forge->dropTable('record');
    }
}
