<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Admin extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id' => [
				'type'				=> 'INT',
				'constraint'		=> 9,
				'unsigned'			=> true,
				'auto_increment'	=> true,
			],
			'username' => [
				'type' 				=> 'VARCHAR',
				'constraint'		=> 255,
				'unique' 			=> true
			],
			'nama' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 255,
			],
			'password' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 255
			],
			'no_wa' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 100,
				'default'			=> 'n'
			],
			'role' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 100,
				'default'			=> 'admin',
			],
			'aktif' => [
				'type'				=> 'CHAR',
				'constraint'		=> 1,
				'default'			=> 'n'
			],
			'arsip' => [
				'type'				=> 'CHAR',
				'constraint'		=> 1,
				'default'			=> 'n'
			],
			'created_at' => [
				'type'				=> 'DATETIME',
				'null'				=> TRUE
			],
			'updated_at' => [
				'type'				=> 'DATETIME',
				'null'				=> TRUE
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('admin');
    }

    public function down()
    {
		$this->forge->dropTable('admin');
    }
}
