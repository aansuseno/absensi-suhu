<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Siswa extends Migration
{
    public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' 			=> 'INT',
				'constraint'	=> 9,
				'auto_increment'=> true,
				'unsigned'		=> true,
			],
			'nis' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unique' 		=> true
			],
			'token' => [
				'type'			=> 'VARCHAR',
				'constraint'	=> 255,
				'unique' 		=> true
			],
			'nama' => [
				'type'			=> 'VARCHAR',
				'constraint'	=> 255
			],
			'kelas' => [
				'type'			=> 'VARCHAR',
				'constraint'	=> 255
			],
			'admin_id' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unsigned'		=> true,
			],
			'arsip' => [
				'type' 			=> 'INT',
				'constraint'	=> 1,
				'default'		=> 0,
			],
			'created_at' => [
				'type' 			=> 'DATETIME',
				'null'			=> true,
			],
			'updated_at' => [
				'type' 			=> 'DATETIME',
				'null'			=> true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('admin_id', 'admin', 'id', 'RESTRICT', 'RESTRICT');
		$this->forge->createTable('siswa');
    }

    public function down()
    {
		$this->forge->dropTable('siswa');
    }
}
