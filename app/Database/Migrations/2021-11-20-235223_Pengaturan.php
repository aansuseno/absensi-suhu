<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pengaturan extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id' => [
				'type' 			=> 'INT',
				'constraint'	=> 9,
				'auto_increment'=> true,
				'unsigned'		=> true,
			],
			'nama_sekolah' => [
				'type'			=> 'VARCHAR',
				'constraint'	=> 100,
			],
			'created_at' => [
				'type' 			=> 'DATETIME',
				'null'			=> true,
			],
			'updated_at' => [
				'type' 			=> 'DATETIME',
				'null'			=> true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('pengaturan');
    }

    public function down()
    {
		$this->forge->dropTable('pengaturan');
    }
}
